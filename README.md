Coding Puzzle:

**Visualization:**

[Google Docs](https://docs.google.com/drawings/d/1okRhNmacdjPbY6FY2bh289rWbk_gnba8W2tcZPqlXCk/edit?usp=sharing)

**To Run:** 
cd jar;
java -d64 -Xms512m -Xmx1g -jar BubbleTea.jar <ARGS>;