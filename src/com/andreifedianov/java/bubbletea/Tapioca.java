package com.andreifedianov.java.bubbletea;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Tapioca implements Runnable {

	private String ingredients;
	private String sortedIngredients;
	private long counter = 1;
	private char tempDis;
	@Override
	public void run() {
		long then = System.currentTimeMillis();
		sortedIngredients = orderTheIngredients();
		System.out.println("Ordered=" + new String(sortedIngredients));
		char[] temp = sortedIngredients.toCharArray();
		doUsefulThings(temp, 0);
		long now = System.currentTimeMillis();
		long total = now - then;
		System.out.println("Time to run=" + total + "ms");
	}


	private void doUsefulThings(char[] ingredients, int index){
		try {
			int i;
			if(index == ingredients.length){
				counter++;
				if(new String(ingredients).equalsIgnoreCase(this.ingredients)){
					System.out.print(counter + "\t");
					System.out.println(ingredients);
				}	
			} else {
				for(i = index; i < ingredients.length; i++){
					if(i != index)
						ingredients = swappadotcom(ingredients, i, index);
					doUsefulThings(ingredients, index + 1);
					if(i != index)
						ingredients = swappadotcom(ingredients, i, index);
				}
			}
		} catch (Exception e) {
		}
	}

	private char[] swappadotcom(char[] cherio, int dis, int dat) {
		try {
			if(dis != dat){ //Redundant
				tempDis = cherio[dis];
				cherio[dis] = cherio[dat];
				cherio[dat] = tempDis;
			}
			return cherio;
		} catch (Exception e) {
		}
		return cherio;
	}

	private String orderTheIngredients() {
		List<String> temp = new ArrayList<String>();
		for (int i = 0; i < ingredients.length(); i++) {
			temp.add("" + ingredients.charAt(i));
		}
		Collections.sort(temp);
		StringBuilder derp = new StringBuilder();
		for (String herp : temp) {
			derp.append(herp);
		}
		return derp.toString();
	}

	public void setAlphabet(String alphabet) {
		this.ingredients = alphabet;
//		System.out.println("Alphabet=" + alphabet);
	}

}
