package com.andreifedianov.java.bubbletea;
import static com.andreifedianov.java.bubbletea.Utils.*;
public class BubbleTea {
	private static Tapioca combo;

	public static void main(String[] args) {
		if(empty(args)){
			if(empty(args[0])){
				System.out.println("Need ingredients!");
				System.exit(0);
			}
			System.out.println("Invalid ingredients!");
			System.exit(0);
		}
		System.out.println("Ingredients=" + args[0]);
		combo = new Tapioca();
		combo.setAlphabet(args[0]);
		combo.run();
	}
}
